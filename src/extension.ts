import * as vscode from 'vscode';

/**
 * This dictionary maps a project/folder path to a chosen color theme.
 */
type ProjectThemes = {
	[projectPath: string]: string | undefined;
};

/**
 * Flag to indicate that the theme change is initiated by this extension,
 * so we don't trigger re-entrant logic when onDidChangeConfiguration fires.
 */
let isSettingTheme = false;

export function activate(context: vscode.ExtensionContext) {
	console.log('PrismProjects extension activated!');

	// 1. Apply or prompt for theme on activation
	setThemesForAllOpenProjects(context);

	// 2. Listen for newly added/removed workspace folders
	const folderChangeSub = vscode.workspace.onDidChangeWorkspaceFolders(() => {
		setThemesForAllOpenProjects(context);
	});

	// 3. Listen for user changes to the color theme.
	//    If the user changes the theme manually, we update our stored theme for the "current" project.
	const configChangeSub = vscode.workspace.onDidChangeConfiguration(e => {
		if (e.affectsConfiguration('workbench.colorTheme') && !isSettingTheme) {
			updateThemeForCurrentProject(context);
		}
	});

	// Register these disposables so they get cleaned up if the extension deactivates
	context.subscriptions.push(folderChangeSub, configChangeSub);
}

/**
 * Iterates over all workspace folders. For each folder:
 *  - If we have a saved theme for that folder path, apply it at workspace scope.
 *  - Otherwise, prompt the user for a theme, store it, and then apply it.
 */
async function setThemesForAllOpenProjects(context: vscode.ExtensionContext) {
	const workspaceFolders = vscode.workspace.workspaceFolders;
	if (!workspaceFolders || workspaceFolders.length === 0) {
		console.log('No open workspace folder(s).');
		return;
	}

	const projectThemes = getProjectThemes(context);

	// Note: This code sets a single theme for each "first folder" in each workspace window.
	// If multiple folders are opened in a SINGLE workspace, they share the same .vscode/settings.json.
	for (const folder of workspaceFolders) {
		const folderPath = folder.uri.fsPath;
		const savedTheme = projectThemes[folderPath];

		if (savedTheme) {
			// We have a stored theme for this folder, so apply it
			await applyWorkspaceTheme(savedTheme);
		} else {
			// Prompt user to pick a theme for this new/unknown folder
			const pickedTheme = await promptForTheme();
			if (pickedTheme) {
				projectThemes[folderPath] = pickedTheme;
				await updateProjectThemes(context, projectThemes);
				await applyWorkspaceTheme(pickedTheme);
				vscode.window.showInformationMessage(
					`Color theme set to "${pickedTheme}" for project: ${folderPath}`
				);
			}
		}
	}
}

/**
 * When the user changes the color theme manually, update our stored value
 * for the first open workspace folder. (Adjust if you want multi-root logic.)
 */
async function updateThemeForCurrentProject(context: vscode.ExtensionContext) {
	const workspaceFolders = vscode.workspace.workspaceFolders;
	if (!workspaceFolders || workspaceFolders.length === 0) {
		return;
	}

	const folderPath = workspaceFolders[0].uri.fsPath;
	const projectThemes = getProjectThemes(context);

	// Get the current theme from VS Code config
	const currentTheme = vscode.workspace.getConfiguration().get<string>('workbench.colorTheme');
	if (currentTheme) {
		projectThemes[folderPath] = currentTheme;
		await updateProjectThemes(context, projectThemes);

		console.log(`Theme updated for "${folderPath}" -> "${currentTheme}"`);
		vscode.window.showInformationMessage(
			`Color theme updated to "${currentTheme}" for project: ${folderPath}`
		);
	}
}

/**
 * Applies the theme at the WORKSPACE level, so that only this workspace is affected
 * (assuming each project is opened in its own window).
 */
async function applyWorkspaceTheme(themeName: string): Promise<void> {
	isSettingTheme = true;
	try {
		await vscode.workspace.getConfiguration().update(
			'workbench.colorTheme',
			themeName,
			vscode.ConfigurationTarget.Workspace
		);
	} finally {
		isSettingTheme = false;
	}
}

/**
 * Helper to gather all contributed theme labels from installed extensions,
 * then prompt the user to pick one.
 */
async function promptForTheme(): Promise<string | undefined> {
	const allThemeLabels = vscode.extensions.all.flatMap(ext =>
		ext.packageJSON.contributes?.themes?.map((theme: any) => theme.label) || []
	);

	return vscode.window.showQuickPick(allThemeLabels, {
		placeHolder: 'Select a color theme for this project',
	});
}

/**
 * Loads the stored project->theme mapping from globalState, or returns an empty object if missing.
 */
function getProjectThemes(context: vscode.ExtensionContext): ProjectThemes {
	return context.globalState.get<ProjectThemes>('prismProjects_themes') || {};
}

/**
 * Stores the updated project->theme mapping back into globalState.
 */
async function updateProjectThemes(context: vscode.ExtensionContext, projectThemes: ProjectThemes) {
	await context.globalState.update('prismProjects_themes', projectThemes);
}

export function deactivate() {
}
