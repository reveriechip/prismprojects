# PrismProjects

PrismProjects is a Visual Studio Code extension that allows users to set a unique color theme for each project. With PrismProjects, switching between projects automatically switches the VSCode color theme to your predefined choice, enhancing the visual distinction and organization of your workspaces. It currently does not support cross device synchronization of themes, though this may be added in the future.

## Features

- **Project-Specific Themes**: Set a distinct color theme for each of your projects.
- **Automatic Theme Switching**: Opening a project automatically applies its associated color theme.
- **Manual Theme Update**: Easily change and update the color theme for any project, with the extension remembering your choice.

## Installation

1. Open Visual Studio Code.
2. Press `Ctrl+P` (`Cmd+P` on macOS) to open the Quick Open dialog.
3. Type `ext install jeffrey-chipman.prismprojects` to find the extension.
4. Click the **Install** button, then the **Enable** button.

## Usage

- **Setting a Theme for a Project**: The first time you open a project with PrismProjects installed, you'll be prompted to select a color theme. This theme will automatically be associated with your project.
- **Changing a Project's Theme**: If you wish to change the currently associated theme, you can do so by manually changing the theme through VSCode's theme selection. PrismProjects will automatically update the project's associated theme to your new selection.

## Requirements

Visual Studio Code version 1.86.0 or higher.

## Known Issues

Currently, there are no known issues. Please report any bugs or issues you encounter on the GitHub issues page.

## Contributing

Contributions are welcome! Please see our GitHub repository for contributing guidelines.

## License

This project is licensed under the MIT License - see the LICENSE file for details.

## Acknowledgments

- Thanks to the Visual Studio Code team and community for their extensive documentation and support.
